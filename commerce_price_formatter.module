<?php

/**
 * @file
 * Provides functionality for handling price format alteration.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_field_formatter_settings_summary_alter().
 *
 * Shows in the add-to-cart summary whether or not the wishlist is enabled.
 */
function commerce_price_formatter_field_formatter_settings_summary_alter(&$summary, $context) {
  /** @var \Drupal\Core\Field\FormatterInterface $formatter */
  if ($context['formatter']->getPluginId() == 'commerce_price_calculated') {
    if ($context['formatter']->getThirdPartySetting('commerce_price_formatter', 'commerce_price_formatter')) {
      $summary[] = new TranslatableMarkup('Discount format is enabled.');
    }
  }
}

/**
 * Implements hook_field_formatter_third_party_settings_form().
 */
function commerce_price_formatter_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, $form, FormStateInterface $form_state) {
  $element = [];
  if ($plugin->getPluginId() == 'commerce_price_calculated') {
    $element['commerce_price_formatter_label'] = [
      '#markup' => '<b>' . new TranslatableMarkup('Discount format settings') . '</b>',
    ];
    $element['commerce_price_formatter'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Enable discount format for calculated price'),
      '#default_value' => $plugin->getThirdPartySetting('commerce_price_formatter', 'commerce_price_formatter'),
    ];
  }
  return $element;
}

/**
 * Implements hook_theme().
 */
function commerce_price_formatter_theme($existing, $type, $theme, $path) {
  return [
    'commerce_price_formatter' => [
      'variables' => ['viewData' => []],
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function commerce_price_formatter_preprocess_commerce_price_calculated(&$variables) {
  $entity = $variables['purchasable_entity'];
  $view_mode = 'default';
  $entity_display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode);
  $field_display = $entity_display->getComponent('price');
  if (
    !empty($field_display['third_party_settings']['commerce_price_formatter']['commerce_price_formatter']) &&
    $field_display['third_party_settings']['commerce_price_formatter']['commerce_price_formatter'] == TRUE
  ) {
    $basePrice = $variables['result']->getBasePrice()->getNumber();
    $calculatePrice = $variables['result']->getCalculatedPrice()->getNumber();
    $appliedDiscount = NULL;
    if ($basePrice != (int) $calculatePrice) {
      $caldiff = round((($basePrice - $calculatePrice) / $basePrice) * 100);
      $appliedDiscount = $caldiff . '%';
    }
    $content = [];
    $content['base_price'] = round($basePrice, 2);
    $content['calculated_price'] = $calculatePrice;
    $content['applied_discount'] = $appliedDiscount;
    if ($basePrice != $calculatePrice) {
      $variables['calculated_price'] = [
        '#theme' => 'commerce_price_formatter',
        '#viewData' => $content,
      ];
      $variables['#cache']['tags'][] = 'commerce_product_variation:' . $variables['purchasable_entity']->id();
    }
    $variables['#attached']['library'][] = 'commerce_price_formatter/format';
  }
}
