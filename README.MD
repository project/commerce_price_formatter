
Commerce Price formatter

The Commerce Price formatter provide a feature to display the discounted price to the front end user.

This module is backend configurable on calculated price widget format settings.

Submit bug reports and feature suggestions, or track changes in the

issue queue.

Table of contents (TOC)

---

* Requirements
* Installation
* Configuration
* Maintainers

REQUIREMENTS

---

This module requires the following modules:

* Commerce ([https://www.drupal.org/project/commerce](https://www.drupal.org/project/commerce))

INSTALLATION

---

* Install as you would normally install a contributed Drupal module. Visit

  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

CONFIGURATION

---

To use:

    1. Navigate to Administration > Commerce > Configuration > Product variation types > display

    2. Change the format for price field to calculated (Format) and In the widget settings check the "Apply discount format to calculated price".

    3. View the Product page to confirm the changes.

**Note: Please clear cache as this is template update**
